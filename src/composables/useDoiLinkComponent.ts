// useDoiLinkComponent.js or useDoiLinkComponent.ts if using TypeScript

import { computed } from 'vue';
import { useFormat, DoiLinkStyles } from './useDoiLinkFormat.ts'; // Adjust the import path as needed
// Import all your DOI link components
import DOILinkOriginal from '../components/DOILinkOriginal.vue';
import DOILinkSrTitleLinkFirst from '../components/DOILinkSrTitleLinkFirst.vue';
import DOILinkTitleAsAriaLabel from '../components/DOILinkTitleAsAriaLabel.vue';
import DoiLinkXrefDoiButton from '../components/DoiLinkXrefDoiButton.vue';
import DOILinkXrefDoiLogo from '../components/DOILinkXrefDoiLogo.vue';

const useDoiLinkComponent = () => {
    const { format } = useFormat(); // Assuming useFormat provides a reactive 'format' ref

    const doiLinkComponent = computed(() => {
        switch (format.value) {
            case DoiLinkStyles.Original:
                return DOILinkOriginal;
            case DoiLinkStyles.SrTitleLinkFirst:
                return DOILinkSrTitleLinkFirst;
            case DoiLinkStyles.TitleAsAriaLabel:
                return DOILinkTitleAsAriaLabel;
            case DoiLinkStyles.CrossrefDoiButton:
                return DoiLinkXrefDoiButton;
            case DoiLinkStyles.CrossrefDoiLogo:
                return DOILinkXrefDoiLogo;
            default:
                return DOILinkOriginal; // Default case to handle undefined or unexpected values
        }
    });

    return { doiLinkComponent };
};

export default useDoiLinkComponent;

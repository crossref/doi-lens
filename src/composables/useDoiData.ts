import { ref, Ref } from 'vue';
import axios from 'axios';

interface Author {
    name: string;
    orcid?: string;
}

interface License {
    URL: string;
}

interface Resource {
    URL: string;
    'content-type'?: string;
}

interface DoiData {
    title: string | undefined;
    licenses: License[] | undefined;
    authors: Author[];
    resources: Resource[] | undefined;
}

// Function to fetch DOI data
async function fetchDoiData(doi: string): Promise<DoiData | null> {
    try {
        const response = await axios.get(`https://doi.org/${doi}`, {
            headers: { 'Accept': 'application/vnd.citationstyles.csl+json' }
        });
        return response.data;
    } catch (error) {
        console.error("Error fetching DOI data:", error);
        return null;
    }
}

// Composable function
export function useDoiData(doi: string): Ref<DoiData | null> {
    const doiData: Ref<DoiData | null> = ref(null);

    // Method for parsing fetched data
    const parseDoiData = (data: any): DoiData => {
        return {
            title: data.title,
            licenses: data.license,
            authors: data.author?.map((author: any) => ({
                name: author.family ? `${author.given} ${author.family}` : author.literal,
                orcid: author.ORCID
            })) || [],
            resources: data.link
        };
    };

    // Fetch and parse DOI data
    (async () => {
        if (doi) {
            const data = await fetchDoiData(doi);
            if (data) {
                doiData.value = parseDoiData(data);
            }
        }
    })();

    return doiData;
}

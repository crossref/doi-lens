import {ref} from "vue";

export enum DoiLinkStyles {
    Original,
    SrTitleLinkFirst,
    TitleAsAriaLabel,
    CrossrefDoiButton,
    CrossrefDoiLogo
}
const format = ref<DoiLinkStyles>(DoiLinkStyles.Original)

const setFormat = (newFormat: DoiLinkStyles) => {
    format.value = newFormat;
};

const useFormat = () => {
    // Directly returning the ref allows it to be used reactively in components.
    // Components using this will automatically update when `format` changes.
    return { format, setFormat };
};

export { format, setFormat, useFormat };

// Default export for the primary expected usage pattern
export default useFormat;